const gulp = require('gulp');
const jsdoc = require('gulp-jsdoc3');
const srcCode = ['./ugly.js'];

gulp.task('doc', function (cb) {
    let config = require('./jsDocConfig');
    gulp.src(srcCode, {read: false})
        .pipe(jsdoc(config, cb));
});

gulp.task('watch', function() {
    gulp.watch(srcCode, ['doc']);
})



