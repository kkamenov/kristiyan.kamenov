module.exports = require('utils')
	  .module('custompreferences', [ 'dw/system','dw/object' ], function(dwsystem, dwobject) {

		var _PREFIXID = 'customsitepreference_',
			currentSite = dwsystem.Site.getCurrent(),
			isMultiCurrencySite = currentSite.getCustomPreferenceValue('isMultiCurrencySite');
	
	
	
		return {
			isMultiCurrencySite: isMultiCurrencySite,
		
			/**
			 * @description Stoms Preference Object
			 * @param {String} locale - The locale
			 * @returns {object} The preferences of a current site
			 */
			stomPreferenceObject: function(locale) {
				if (isMultiCurrencySite)
					return dwobject.CustomObjectMgr.getCustomObject('customSitePreferences', _PREFIXID + locale) ||
						currentSite.preferences;
					return currentSite.getPreferences();
			},
		

			/**
			 * @description Gets Custom Preference Value
			 * @param {number} attributeID - The Id of the attribute
			 * @param {String} locale - The locale
			 * @returns {number} The custom preference value
			 */
			getCustomPreferenceValue: function(attributeID, locale) {
				if (isMultiCurrencySite) {
					let co = dwobject.CustomObjectMgr.getCustomObject('customSitePreferences', _PREFIXID + (locale || request.locale));

					// use Site CustomPreferenceValue if custom object not found or attribute is empty
					if (empty(co) || !(attributeID in co.custom) || (empty(co.custom[attributeID]) && typeof (co.custom[attributeID]) !== 'boolean')) {
						return currentSite.getCustomPreferenceValue(attributeID);
					} //end of inner if

					return co.custom[attributeID];
				} // end of outer if
				return currentSite.getCustomPreferenceValue(attributeID);
			},
		
			/**
			 * @description Get custom preference json value
			 * @param {number} attributeID - The ID of the attribute
			 * @param {string} locale - The locale
			 * @returns {(object|string)} Returns object if it succeed or logs error in the logger
			 */
			getCustomPreferenceJsonValue: function(attributeID, locale) {
				var value = this.getCustomPreferenceValue(attributeID, locale);
				try {
					return JSON.parse(value);
				} catch (e) {
					dwsystem.Logger.error('getCustomPreferenceJsonValue: Cannot parse JSON for {0} attribute: {1}', attributeID, e.message || JSON.stringify(e));
				}
			},
		
			/**
			* @description Gets the default list price
			* @returns {(null|object)} Returns null or custom preference value
			*/
			getListPriceDefault: function() {
				if (currentSite.getCustomPreferenceValue('enableGEOLocatedPrices')) {
					return null;
				}
				return this.getCustomPreferenceValue('listPriceDefault');
			},
		
			/**
			 * @description Gets Locale From the Given ID
			 * @param {string} id - Specified ID as string
			 * @returns {string} Returns substring of the specified ID
			 */
			getLocaleFromID: function(id) {
				return id.substr(_PREFIXID.length);
			},
		
			/**
			 * @description Gets the Custom Organization Preference Json Value
			 * @param {number} attributeID - the ID of the attribute
			 * @returns {(object|string)} Returns object (if succeed) or Logs error in the Logger (if fails)
			 */
			getCustomOrganizationPreferenceJsonValue: function(attributeID) {
				var value = dwsystem.System
									.getPreferences()
									.custom[attributeID];

				try {
					return JSON.parse(value);
				} catch (e) {
					dwsystem.Logger.error('getCustomOrganizationPreferenceJsonValue: Cannot parse JSON for {0} attribute: {1}', attributeID, e.message || JSON.stringify(e));
				}
			},
		
			/**
			 * @description Gets the Custom Organization Preference Value
			 * @param {number} attributeID - the ID of the attribute
			 * @returns {(number|null)} Returns custom preferences id or NULL
			 */
			getCustomOrganizationPreferenceValue: function(attributeID) {
				var customPreferences = dwsystem.System
												.getPreferences()
												.custom;

				return (attributeID in customPreferences) && (customPreferences[attributeID] || null);
			}
	};
});
