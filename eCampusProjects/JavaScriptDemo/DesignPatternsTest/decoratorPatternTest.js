function Sale(price) { //constructor
    this.price = price || 0;
}

Sale.prototype.getPrice = function() {
    return this.price;
}

Sale.prototype.decorate = function(decorator) {
    var F = function() {},
        overrides = this.constructor.decorators[decorator],
        i,
        newObj;

    F.prototype = this;
    newObj = new F();
    newObj.uber = F.prototype;
    for (i in overrides) {
        if (overrides.hasOwnProperty(i)) {
            newObj[i] = overrides[i];
        }
    }
    return newObj

}

Sale.decorators = {};

Sale.decorators.fedTax = {
    getPrice: function() {
        var price = this.uber.getPrice();
        price += price * 5 / 100;
        return price;
    }
}

Sale.decorators.quebec = {
    getPrice: function() {
        var price = this.uber.getPrice();
        price += price * 7.5 / 100;
        return price;
    }
}

//testing the above code
var sale = new Sale(100);
sale = sale.decorate('fedTax');
sale = sale.decorate('quebec');
console.log(sale.getPrice());
