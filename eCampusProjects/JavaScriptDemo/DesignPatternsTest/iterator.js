let iter = (function() {
    let arr = [1, 2, 3, 4, 5, 7],
        index = 0,
        len = arr.length;
    
    function hasNext() {
        return index < len;
    }

    function next() {
        if (this.hasNext()) {
            let element = arr[index];
            index++;
            return element;

        } else {
            return null;
        }
    }

    function rewind() {
        index = 0;
    }

    module.exports = {
        hasNext,
        next,
        rewind
    }
    
}())