$(document).ready(function() {
    $('#btn').on('click', function() {
        var taskName = $('#tb-taskname').val()
        var ul = $('#items-list')
        var li = $('<li></li>')
           
        li.html('<label><input type="checkbox"><span class="span-text">' + taskName + '</span></label>')

        var btn = $('<button class="btn-edit">Edit</button>')
        var btnDelete = $('<button class="btn-delete">Delete</button>')
        var input = $('<input type="text" class="tb-edit">')
        input.appendTo(li)
        input.hide()
        btn.appendTo(li)
        btnDelete.appendTo(li)
        li.appendTo(ul)

        var task = new TaskManager.Task(taskName)

        li.attr('data-id', task.id)
        task.saveToStorage()
        console.log(task)
        $('#tb-taskname').val('')

    })

    $('#items-list').on('click', '.btn-edit', function() {
        $(this).siblings('label').hide()
        $(this).prev().show()
        $(this).hide()
        $(this).prev().focus()
        $(this).next().hide()
    })

    $('#items-list').on('keyup', '.tb-edit', function(ev) {
        if (ev.keyCode == 13) {
            if ($(this).val().length >= 3) {

                $(this).prev().find('.span-text').text($(this).val())
                $(this).prev().show()
                $(this).hide()
                $(this).next().show()
                $(this).siblings('.btn-delete').show()

                //the backend logic 
                TaskManager.editTaskName($(this).parent().attr('data-id'), $(this).val())
                $(this).val('')
            }
        }
    })

    //on statechange event for checkboxes
    $('#items-list').on('change', 'input[type="checkbox"]', function() {
        toastr.success('Task Completed')
        //$(this).parents('li[data-id]').remove();

        //TODO: REMOVE THE TASK FROM THE STORAGE
    })
})
