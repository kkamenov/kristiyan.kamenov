var TaskManager = (function() {
    var id = 0
    var storage = {}

    function Task(name) {
        this.name = name
        this.done = false
        this.id = ++id
    }
    

    Task.prototype.saveToStorage = function() {
        storage[this.id] = this
    }

    Task.prototype.toString = function() {
        return `TaskName: ${this.name} | Done: ${this.done} | ID: ${this.id}`
    }

    function getTask(id) {
        return storage[id]
    }

    function editTaskName(id, newName) {
        var currTask = storage[id]
        currTask.name = newName
    }

    function removeTask(id) {
        delete storage[id]
    }

    function setDone(id, boolVal) {
        if (isNaN(id) || typeof boolVal !== 'boolean') {
            return
        }
        var currTask = storage[id]
        currTask.done = boolVal
    }

    

    /* tests */
    /*
    var task1 = new Task('pesho')
    var task2 = new Task('gosho')
    console.log('Creating task1: ' + task1.toString())
    console.log('Creating task2: ' + task2.toString())

    task2.saveToStorage() // save task2 to storage
    task1.saveToStorage() // save task1 to storage
    console.log('Task1 from storage: ' + storage[1].toString()) // log the saved task from the storage

    var task2UsingGetTask = getTask(2) // log task2 using the getTask func
    console.log(task2UsingGetTask.toString()) // --||--

    editTaskName(2, 'EDITED') // edit the task2's name
    console.log(task2.toString()) // log into the console

    setDone(1, true) // change the done prop to true
    console.log('DONE TRUE: ' + task1.toString()) // log to console

    removeTask(1) // REMOVE TASK1 FROM THE STORAGE
    console.log(storage[1]) // should print undefined
    */

    return {
        Task,
        createTask: function (name) {
            return {
                name,
                done: false,
                id: ++id
            }
        },
        getTask,
        editTaskName,
        removeTask,
        setDone
    }
    
}())