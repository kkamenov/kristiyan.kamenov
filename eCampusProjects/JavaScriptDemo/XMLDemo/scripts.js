window.onload = function() {

    var xml = document.getElementById('tb-xml').innerText

    function XMLParseTo(type) {
        let parser = new DOMParser()
        let xmlDoc = parser.parseFromString(xml, 'text/xml')
        let json = []
        let csv = []
        csv.push('Title,Pz_Provider,Price,Group,ID\n')
        let items = xmlDoc.getElementsByTagName('Item'),
            itemsLen = items.length,
            resultArea = document.getElementById('tb-result')
        resultArea.innerText = ''

        for (let i = 0; i < itemsLen; i++) {
            let currObj = {}

            //extract the lin_msgs from every item
            let item_msgs = items[i].getElementsByTagName('Lin_Msg');
            for (let j = 0, len = item_msgs.length; j < len; j++) {
                let lin_msg_text = item_msgs[j].getAttribute('lin_msg_text');
                if (lin_msg_text.indexOf('title') !== -1) {
                    let splitted = lin_msg_text.split('=')
                    let title = splitted[1]
                    currObj.title = title
                    //configuring csv
                    //if we have a title (we have it in all cases)
                    if (title) {
                        //escape the comma in the title
                        title = title.replace(',', '')
                        csv.push(title + ',')
                    }
                    
                }
                else if (lin_msg_text.indexOf('pz_provider') !== -1) {
                    let splitted = lin_msg_text.split('=')
                    let pzProvider = splitted[1]
                    currObj.pz_provider = pzProvider
                    //configuring csv
                    //if we have a pz_provider
                    if (pzProvider) {
                        csv.push(pzProvider + ',')
                    }
                } 
                //if we dont have a pz_provider
                else {
                    csv.push("")
                }
            }
            //json
            currObj.price = items[i].getAttribute('actual_price')
            //csv
            csv.push(items[i].getAttribute('actual_price')+',')

            //json
            currObj.group = items[i].getAttribute('cord_group')

            //csv 
            csv.push(items[i].getAttribute('cord_group')+',')

            //json
            currObj.id = items[i].getAttribute('item_id')

            //csv
            csv.push(items[i].getAttribute('item_id'))
            csv.push('\n')

            json.push(currObj);
        }
        
        if (type === 'json') {
            resultArea.innerText += JSON.stringify(json);
        } else if (type === 'csv') {
            resultArea.innerText = csv.join('')
        }
    }
    
    //dom events
    document.getElementById('btn-parse-json').addEventListener('click', () => XMLParseTo('json'))
    document.getElementById('btn-parse-csv').addEventListener('click', () => XMLParseTo('csv'))
}


